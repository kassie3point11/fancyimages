package dev.kassiemyers.minecraft.fancyimages;

import com.bergerkiller.bukkit.common.map.MapDisplay;
import com.bergerkiller.bukkit.common.map.MapTexture;
import com.bergerkiller.bukkit.common.utils.ItemUtil;
import org.bukkit.inventory.ItemStack;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.net.URL;

public class FancyImageMapDisplay extends MapDisplay {
    private MapTexture imagemapImage;

    @Override
    public void onAttached() {
        var url = properties.get("url", String.class);
        fetchTexture(url);
    }

    public static ItemStack createMap(String url) {
        var item = FancyImageMapDisplay.createMapItem(FancyImageMapDisplay.class);
        var tag = ItemUtil.getMetaTag(item, true);
        tag.putValue("url", url);
        ItemUtil.setMetaTag(item, tag);
        return item;
    }

    private void fetchTexture(String url) {
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> {
            try {
                imagemapImage = MapTexture.fromImage(ImageIO.read(new URL(url)));
            } catch (IllegalArgumentException ex) {
                imagemapImage = MapTexture.createEmpty(128, 128);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            plugin.getServer().getScheduler().runTask(plugin, this::applyTexture);
        });
    }

    private void applyTexture() {
        getLayer().draw(imagemapImage, 0, 0);
    }


}
