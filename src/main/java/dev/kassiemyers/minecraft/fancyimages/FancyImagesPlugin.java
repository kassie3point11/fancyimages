package dev.kassiemyers.minecraft.fancyimages;

import dev.jorel.commandapi.CommandAPI;
import org.bukkit.plugin.java.JavaPlugin;

public class FancyImagesPlugin extends JavaPlugin {
    @Override
    public void onEnable() {
        getLogger().info("Enabling FancyImages");
        CommandAPI.registerCommand(FancyImageCommand.class);
    }
}
