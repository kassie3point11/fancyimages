package dev.kassiemyers.minecraft.fancyimages;

import dev.jorel.commandapi.annotations.Command;
import dev.jorel.commandapi.annotations.Default;
import dev.jorel.commandapi.annotations.Permission;
import dev.jorel.commandapi.annotations.Subcommand;
import dev.jorel.commandapi.annotations.arguments.AIntegerArgument;
import dev.jorel.commandapi.annotations.arguments.AIntegerRangeArgument;
import dev.jorel.commandapi.annotations.arguments.AMultiLiteralArgument;
import dev.jorel.commandapi.annotations.arguments.AStringArgument;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@Command("fancyimages")
public class FancyImageCommand {

    @Default
    public static void Help(CommandSender sender) {
        sender.sendMessage("--- FancyImages Help ---");
        sender.sendMessage("/fancyimage - this help");
        sender.sendMessage("/fancyimage create <url> [scalex/scaley/none] [size-in-maps-to-scale-by] - Create an image map");
    }

    @Subcommand("create")
    @Permission("fancyimages.create")
    public static void createMap(Player player,
                                 @AStringArgument String url) {
        createMap(player, url, "none", 1);
    }

    @Subcommand("create")
    @Permission("fancyimages.create")
    public static void createMap(Player player,
                                 @AStringArgument String url,
                                 @AMultiLiteralArgument({"scalex", "scaley", "none"}) String scaleDimension,
                                 @AIntegerArgument(min = 1) int scaleAmount) {
        player.getInventory().addItem(FancyImageMapDisplay.createMap(url));
        player.sendMessage("Map given!");
    }
}
