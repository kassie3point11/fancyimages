import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import net.minecrell.pluginyml.bukkit.BukkitPluginDescription

plugins {
    java
    id("net.minecrell.plugin-yml.bukkit") version "0.5.2"
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

repositories {
    maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
    maven("https://oss.sonatype.org/content/repositories/snapshots")
    maven("https://oss.sonatype.org/content/repositories/central")
    maven("https://ci.mg-dev.eu/plugin/repository/everything")
    maven("https://libraries.minecraft.net")
    maven("https://repo.codemc.org/repository/maven-public/")
    mavenCentral()
}

dependencies {
    // Pick only one of these and read the comment in the repositories block.
    compileOnly("org.spigotmc:spigot-api:1.19-R0.1-SNAPSHOT") // The Spigot API with no shadowing. Requires the OSS repo.
    compileOnly("com.bergerkiller.bukkit:BKCommonLib:1.19-v1")
    implementation("dev.jorel:commandapi-core:8.4.1")
    compileOnly("dev.jorel:commandapi-annotations:8.4.1")
    annotationProcessor("dev.jorel:commandapi-annotations:8.4.1")
}

bukkit {
    main = "dev.kassiemyers.minecraft.fancyimages.FancyImagesPlugin"
    apiVersion = "1.19"
    authors = listOf("Kassie Myers")
    depend = listOf("BKCommonLib")
    defaultPermission = BukkitPluginDescription.Permission.Default.OP

//    commands {
//        register("create") {
//            description = "create a fancyimage map"
//            permission = "fancyimages.create"
//            usage = "/fancyimage:create <link> [scaleX/scaleY] [max height/width in number of maps]"
//        }
//    }
    permissions {
        register("fancyimages.create") {
            description = "Allows you to create fancyimage maps"
            default = BukkitPluginDescription.Permission.Default.OP
        }
    }
}

tasks.named<ShadowJar>("shadowJar") {
    dependencies {
        include(dependency("dev.jorel:commandapi-annotations:8.4.1"))
    }
    relocate("dev.jorel.commandapi", "dev.kassiemyers.minecraft.fancyimages.commandapi")
}